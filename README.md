# pyDmtDriver

This module is a layer that connects to DWIN DMT modules through a serial port.

It allows the recording and reading of any register of the device. This allows you to work freely with the HMI panel, but requires knowledge of the panel memory organization.


## Requirements

This module require few another modules:
- pySerial
- pyCRC

## Usage

Import module, and create object of `DmtDriver`.
Initialize them with:
- serial port name
- serial port baud rate
- value of registers:
    - R2
    - R3
    - RA

`def __init__(self, sp='/dev/ttyAMA0', br=115200, r2=0x00, r3=0x5A, ra=0xA5):`

If you don't know register values and serial port baud rate, check the 
`CONFIX.txt` file of the DGUS project.

#### Writing data

To write some data to DMT memory use `write(reg, data, rw_cmd=const
.WRITE_SRAM)` function.

Set the starting address as `reg` param, and list of values as `data` param. 
Set the `rw_cmd` param, as `WRITE_REGISTER`or `WRITE_TREND`, if you want to 
write into specific memory type.

Remember that maximum `data` length is 250, and maximum value is 0xFFFF. 

#### Reading data

To read data from memory (register or sram), you have to use `write(reg, data, rw_cmd=const
.WRITE_SRAM)` function first.
Set the `reg` addres to read, empty `data` list and change `rw_cmd` to 
`READ_REGISTER` or `READ_SRAM`.
Next use `read()` function to get response from DMT.

## Defined constants

The `const.py` file defines the following constant values:
- commands
    - `WRITE_REGISTER`
    - `READ_REGISTER`
    - `WRITE_SRAM`
    - `READ_SRAM`
    - `WRITE_TREND`
    
## Copyrights

> Copyrigths (c) 2019 Piotr Płaczek
This file is protected by intellectual property law.
When using this file, please be aware of the terms of the MIT license.
