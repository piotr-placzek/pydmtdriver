"""
* Copyrigths (c) 2019 Piotr Płaczek
* This file is protected by intellectual property law.
* When using this file, please be aware of the terms of the MIT license.
"""

import serial
from PyCRC.CRC16 import CRC16
from DmtDriver import const


class DmtDriver:
    __sp = None
    __r2 = b'\x00'
    __r3 = b'\xA5'
    __ra = b'\x5A'

    def __init__(self, sp='/dev/ttyAMA0', br=115200, r2=0x00, r3=0x5A,
                 ra=0xA5, timeout = 0.1):
        self.__sp = serial.Serial(sp, br, serial.EIGHTBITS,
                                  serial.PARITY_NONE, serial.STOPBITS_ONE, timeout)
        self.__r2 = r2
        self.__r3 = r3
        self.__ra = ra

    def execute(self, cmd, reg, data):
        """
        :param reg: VP address - have to be 16 bit value
        :param data: have to be a list with 16 bit values
        :param cmd:
        :return: for WRITE returns amount of wrote bytes
                 for READ returns respose or
                          amount of wrote bytes if sth goes wrong
        """
        if not isinstance(data, list):
            return False
        fr = [self.__r3, self.__ra, self.__len(cmd, data), cmd]
        fr = fr + self.__splitToBytes(reg)
        if cmd == const.WRITE_SRAM or cmd==const.WRITE_TREND:
            fr = fr + self.__splitToBytes(data)
        else:
            fr = fr + data
        if self.__r2 & 4:
            fr = fr + self.crc16(fr)
        if cmd == const.READ_SRAM or cmd == const.READ_REGISTER:
            wr = self.__sp.write(bytearray(fr))
            if wr:
                return self.__sp.read(258)
            return wr
        return self.__sp.write(bytearray(fr))

    def crc16(self, fr):
        return self.__splitToBytes(CRC16().calculate(fr))

    def __splitToBytes(self, b16):
        if not isinstance(b16, list):
            res = [(b16 >> 8) & 0xFF, b16 & 0xFF]
            return res
        else:
            res = []
            for b in b16:
                res = res + [(b >> 8) & 0xFF, b & 0xFF]
            return res

    def __len(self, cmd, data):
        if cmd == const.WRITE_SRAM:
            return len(data)*2+3
        if cmd == const.READ_SRAM:
            return len(data) * 2 + 2
        if cmd == const.WRITE_REGISTER:
            return len(data) + 2
        if cmd == const.READ_REGISTER:
            return 3

