"""
* Copyrigths (c) 2019 Piotr Płaczek
* This file is protected by intellectual property law.
* When using this file, please be aware of the terms of the MIT license.
"""
WRITE_REGISTER = 0x80
READ_REGISTER = 0x81
WRITE_SRAM = 0x82
READ_SRAM = 0x83
WRITE_TREND = 0x84